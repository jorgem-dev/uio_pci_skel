## UIO PCI Skeleton Driver for Linux 4.4+

### Loading the module

Under ./src run:

```
$ make
$ sudo modprobe uio
$ sudo insmod uio_pci_skel.ko
```

### Unloading the module

```
$ sudo rmmod uio_pci_skel
```
